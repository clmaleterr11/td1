package org.isima.javapro;

import org.isima.javapro.expense.Expense;
import org.isima.javapro.expense.ExpenseReader;
import org.isima.javapro.valid.ExpenseValidator;
import org.isima.javapro.valid.LDAPEmployee;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class ExpenseValidatorTest {

    @Test
    @DisplayName("Test expense validator with invalid expense")
    void testWithInvalidExpense() {
        Expense failedExpense = new Expense("1","Laurent PasEmploye",LocalDate.now(), "Facture","Grosse facture",253.36);


        assertFalse(ExpenseValidator.isValid(failedExpense));
    }

    @Test
    @DisplayName("Test expense validator with valid expense")
    void testWithValidExpense() {
        Expense correctExpense = new Expense("1","Jean-Bernard Compta",LocalDate.now(), "train","Train pour Paris",199.99);


        assertTrue(ExpenseValidator.isValid(correctExpense));
    }

    @Test
    @DisplayName("Retrieve expenses from a remote location")
    void testRetrieveExpenses() {
         assertTrue(ExpenseReader.downloadFile("/tmp/expenses.csv","https://raw.githubusercontent.com/anthoRx/td1/master/expenses.csv"));
    }

    @Test
    @DisplayName("Create expenses from a CSV data")
    void testCreateExpensesFromCsv() {
        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Validate expense's employee exists")
    void testIsEmployeeExists() {
        assertTrue(LDAPEmployee.isEmployeeExists("Christine Errhach"));
    }

    @Test
    @DisplayName("Validate if an expense have a legal amount")
    void testHaveLegalAmount() {
        Expense expense = new Expense("1","Jean-Bernard Compta",LocalDate.now(), "train","Train pour Paris",199.99);

        assertTrue(ExpenseValidator.haveLegalAmount(expense));    }

    @Test
    @DisplayName("Validate if an expense respect enterprise policy")
    void testHavePolicyAmount(){
        Expense expense = new Expense("1","Jean-Bernard Compta",LocalDate.now(), "train","Train pour Paris",199.99);
        assertTrue(ExpenseValidator.havePolicyAmount(expense));
    }

    @Test
    @DisplayName("Validate if employees expenses respect the month policy")
    void testRespectMonthPolicy() {

        fail("Impossible to test it ¯\\_(ツ)_/¯");
    }

    @Test
    @DisplayName("Append valid expenses to expenses history")
    void testSaveValidExpenses(){
        File f = new File(ExpenseValidator.HISTORY_FILE);
        long lDeb = f.length();
        ExpenseValidator.validate();
        f = new File(ExpenseValidator.HISTORY_FILE);
        assertTrue(lDeb < f.length());
    }

    @Test
    @DisplayName("Append invalid expenses to expenses history")
    void testSaveInvalidExpenses() {
        File f = new File(ExpenseValidator.INVALID_HISTORY_FILE);
        long lDeb = f.length();
        ExpenseValidator.validate();
        f = new File(ExpenseValidator.INVALID_HISTORY_FILE);
        assertTrue(lDeb < f.length());
    }
}
