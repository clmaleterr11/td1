package org.isima.javapro.tools;

import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

public class FileTools {
    public static boolean downloadFile(String toFile, String url) {
        boolean success = true;
        try {
            FileUtils.copyURLToFile(new URL(url), new File(toFile), 10000, 10000);
        } catch (IOException e) {
            success=false;
            e.printStackTrace();
        }
        return success;
    }

    public static void saveToFile(List<Object> dataToSave, String file) {
        dataToSave.forEach(data -> {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonString = objectMapper.writeValueAsString(data);
                File historyFile = new File(file);
                if (!Files.exists(historyFile.getParentFile().toPath()))
                    Files.createDirectories(historyFile.getParentFile().toPath());
                if(historyFile.exists())
                    Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.APPEND);
                else
                    Files.write(historyFile.toPath(), Arrays.asList(jsonString), StandardOpenOption.CREATE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

