package org.isima.javapro.valid;

import org.isima.javapro.expense.Expense;
import org.isima.javapro.expense.ExpenseReader;
import org.isima.javapro.tools.FileTools;

import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;

public class ExpenseValidator {

    private static String EXPENSE_FILE_URL = "https://raw.githubusercontent.com/anthoRx/td1/master/expenses.csv";

    public static String HISTORY_FILE = "/history/valid_expenses.json";
    public static String INVALID_HISTORY_FILE = "/history/invalid_expenses.json";

    public static boolean haveLegalAmount(Expense e) {
        return e.getAmount() > 0 && e.getAmount() < Double.MAX_VALUE;
    }

    public static boolean havePolicyAmount(Expense e) {
        Map<String, Double> policy = new HashMap<>();
        policy.put("hotel", 180.0);
        policy.put("repas", 35.0);
        policy.put("train", 200.0);
        policy.put("avion", 800.0);
        policy.put("drinks", 100.0);
        policy.put("boisson", 100.0);

        Double policyValue = policy.get(e.getCategory());
        return policyValue == null || policyValue > e.getAmount();
    }

    public static boolean respectMonthPolicy(List<Expense> expenses) {
        double limit = 1000;
        return expenses.stream()
                .collect(groupingBy(Expense::getMonth, summingDouble(Expense::getAmount)))
                .entrySet()
                .stream()
                .allMatch(entry -> entry.getValue() < limit);
    }

    public static boolean isValid(Expense expense)
    {
        return LDAPEmployee.isEmployeeExists(expense.getEmployee()) && haveLegalAmount(expense) && havePolicyAmount(expense);
    }

    public static void validate() {
        List<Expense> invalidExpenses = new ArrayList<>();
        List<Expense> validExpenses = new ArrayList<>();
        String toFile = "/tmp/expenses.csv";

        FileTools.downloadFile(toFile,EXPENSE_FILE_URL);

        List<Expense> expenses = ExpenseReader.getExpensesFromCsv(toFile);

        expenses.forEach(expense -> {
            if (isValid(expense))
                validExpenses.add(expense);
            else
                invalidExpenses.add(expense);
        });



        Map<String, List<Expense>> expensesByEmployee = validExpenses.stream().collect(groupingBy(Expense::getEmployee));
        List<Expense> expensesToSave = new ArrayList<>();
        expensesByEmployee.forEach((k, v) -> {
            if(respectMonthPolicy(v))
                expensesToSave.addAll(v);
            else
                invalidExpenses.addAll(v);
        });


        FileTools.saveToFile((List)expensesToSave, HISTORY_FILE);
        FileTools.saveToFile((List)invalidExpenses, INVALID_HISTORY_FILE);
    }


}
