package org.isima.javapro;

import org.isima.javapro.valid.ExpenseValidator;


public class ExpenseApp {

    public static void main(String[] args) {
        ExpenseValidator expenseValidator = new ExpenseValidator();

        expenseValidator.validate();
    }
}
