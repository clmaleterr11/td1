package org.isima.javapro.expense;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.isima.javapro.expense.Expense;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExpenseReader {

    public static List<Expense> getExpensesFromCsv(String csvFile){
        List<Expense> expenses = new ArrayList<Expense>();
        CSVFormat csvFormat = CSVFormat.DEFAULT
                .withHeader("id", "employee", "date", "category", "description", "amount")
                .withDelimiter(';')
                .withTrim()
                ;

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(csvFile));

            CSVParser csvParser = new CSVParser(reader, csvFormat);

            for (CSVRecord csvRec : csvParser) {
                if (!"id".equals(csvRec.get("id"))) {
                    LocalDate date = LocalDate.parse(csvRec.get("date"), dateFormatter);
                    double amount = Double.parseDouble(csvRec.get("amount"));

                    Expense expense = new Expense(csvRec.get("id"), csvRec.get("employee"), date, csvRec.get("category"), csvRec.get("description"), amount);

                    expenses.add(expense);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return expenses;
    }
}

